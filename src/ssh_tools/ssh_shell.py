import paramiko
from paramiko import SSHClient
import logging
logger = logging.getLogger(__name__)


class SshShell:

    __client: SSHClient = paramiko.client.SSHClient()
    
    def __init__(self, host: str, username: str, password: str):
        self.__client = paramiko.client.SSHClient()
        self.__client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        self.__client.connect(host, username=username, password=password)

    def send_commands(self, commands: list[str]) -> None:
        for command in commands:
            stdin, stdout,stderr = self.__client.exec_command(command)
            if stdout:
                logger.info(stdout)
            if stderr:
                logger.info(stderr)

    def close_ssh(self)-> None:
        self.__client.close()
        logger.info("closed ssh client")