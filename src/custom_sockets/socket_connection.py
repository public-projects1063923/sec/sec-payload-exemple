import socket
import threading
from threading import Event
import time
import logging
logger = logging.getLogger(__name__)

class SocketConnection(threading.Thread):

    __host: str
    __port: int
    __s: socket
    __s_stop: Event
    __received: str

    def __init__(self, host: str, port: int) -> None:
        threading.Thread.__init__(self)
        self.createSocket()
        self.__host = host
        self.__port = port
        self.__received = ""
        self.__s_stop = Event()
    
    def run(self):
        self.listening(self.__host, self.__port)

    def createSocket(self) -> socket:
        self.__s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.__s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        socket.setdefaulttimeout(6000) # timeout 6000 seconde without any packet received
        return self.__s

    def closeSocket(self)-> None:
        #! TODO fix: thread is not closing
        self.__s_stop.set()
        self.__s.close()
        logger.info("closed Thread & socket")

    def listening(self, host: str, port: int)-> bool:
        self.__s.bind((host, port))
        self.__s.listen(5) # only 5 request
        logger.info(f"Listening on port {port}")
        try:
            while not self.__s_stop.is_set(): 
                # establish connection
                clientSocket, addr = self.__s.accept()
                logger.info(f"Got a connection from {str(addr)}")
                self.__received =+ clientSocket.recv(1024).decode() # utf-8 by default
                logger.info(self.__received)
                # currentTime = time.ctime(time.time()) + "\r\n"
                # clientSocket.send(currentTime.encode('ascii'))
                clientSocket.close()
            logger.info(f"Stop listening on port {port}")
            self.closeSocket()
            return True
        except Exception as error:
            logger.info("Error running thread")
            logger.info(error)
            self.closeSocket()
            return False


    # getters / setters

    @property
    def s(self):
        return self.__s

    @s.setter
    def s(self, new__s):
        self.__s = new__s

    @property
    def received(self):
        return self.__received

    @received.setter
    def received(self, new__received):
        self.__received = new__received