import netifaces
import json
import nmap
from functional import seq as seq
import logging
logger = logging.getLogger(__name__)


class DiagnosticInterface:
        
    __gateways: dict
    __network_and_ifaces: dict
    __debug: bool

    def __init__(self, gateway: dict = {}, network_and_ifaces: dict = {}, debug: bool = False)-> None:
        self.__debug = debug
        self.__gateways_ = gateway if bool(gateway) else netifaces.gateways()
        self.__network_and_ifaces = network_and_ifaces if network_and_ifaces else self.get_interfaces_and_network()
        
    def get_interfaces_and_network(self) -> dict:
        interfaces_ = netifaces.interfaces()
        adresses = {}
        for interface_ in interfaces_:
            adresses[interface_] = netifaces.ifaddresses(interface_)
            # create easy to read dict
            #! TODO find potentiel host ex: 192.168.127.129
            adresses[interface_]["human_readeable"] = \
                {
                "mac_adresses": adresses.get(interface_, {}).get(netifaces.AF_LINK, ""), 
                "iface_addrs_ipv4": adresses.get(interface_, {}).get(netifaces.AF_INET, ""),
                "iface_addrs_ipv6": adresses.get(interface_, {}).get(netifaces.AF_INET6, ""),
                }
        
        seq(adresses)\
            .filter(lambda x: x != "lo" and adresses.get(x).get("human_readeable", {}).get("iface_addrs_ipv4", False) )\
            .flat_map(lambda x: adresses.get(x).get("human_readeable").get("iface_addrs_ipv4"))\
            .for_each(self.scanInterfaces)

        if self.__debug:
            # logger.info(json.dumps(adresses, indent=4))
            print(json.dumps(adresses, indent=4))
        return adresses

    def scanInterfaces(self, ip_dict: dict) -> None:
        #? should we add ipv6
        nmap_scanner = nmap.PortScanner()
        ip_dict.update({"nmap": nmap_scanner.scan(ip_dict.get("addr"), '0-1000', arguments="-sA")})
        ip_dict.update({"all_host": nmap_scanner.all_hosts()})


    # getters / setters

    @property
    def network_and_ifaces(self):
        return self.__network_and_ifaces

    @network_and_ifaces.setter
    def network_and_ifaces(self, new__network_and_ifaces):
        self.__network_and_ifaces = new__network_and_ifaces

# Debug
if __name__ == '__main__':
    diagnostic_interface: DiagnosticInterface = DiagnosticInterface(debug= True)
