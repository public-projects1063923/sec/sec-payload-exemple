import logging
import logging.config


logging.config.fileConfig('./config/logging.conf')

# create logger
logger = logging.getLogger(__name__)
