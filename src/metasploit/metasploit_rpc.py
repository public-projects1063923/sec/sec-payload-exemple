from pymetasploit3.msfrpc import MsfRpcClient, MsfConsole
import os
import subprocess
import time
import logging
logger = logging.getLogger(__name__)

class MetasploitRpc:
    
    __client: MsfRpcClient
    __ms_console: MsfConsole

    def __init__(self, password: str, port: int)-> None:
        """Init connection with metasploit rpc.
        If rpc is down, start the service.
        Args:
            password (str): metasploit rpc password (user: Kali)
            port (int): metasploit rpc port
        """
        try:
            self.__client = MsfRpcClient('kali', port='55552')
            self.__ms_console = MsfConsole(self.__client)
        except Exception as error:
            logger.info(error)
            logger.info("Metasploit rpc down, starting server")
            # # os.system("msfconsole -r ./metasploit/rpc_init.rc &") 
            # cmd = subprocess.Popen("msfconsole -r ./metasploit/rpc_init.rc")
            # cmd.communicate()
            # time.sleep(10) # TODO better event check
            # logger.info("done")
            # os.system("msfconsole -r ./metasploit/rpc_init.rc") # & => background job
            # cmd = subprocess.Popen("msfconsole -r ./metasploit/rpc_init.rc &")
            # cmd.communicate()
    
    def init_db(self) -> None:
        pass

    def bind_db(self) -> None:
        pass