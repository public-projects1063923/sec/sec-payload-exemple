# Create file that can be executed with
# $ msfconsole -r file.txt
def resource_file_buider(host: str, port: int, username: str, password: str) -> None:
    with open('./ssh_login.rc', 'a') as my_file:
        my_file.write("use auxiliary/scanner/ssh/ssh_login\n")
        my_file.write(f"set RHOSTS {host}\n")
        my_file.write(f"set USERNAME {username}\n")
        my_file.write(f"set PASSWORD {password}\n")
        my_file.write("run\n")

# debug
if __name__ == '__main__':
    resource_file_buider("192.168.194.128", 22, "msfadmin", "msfadmin")